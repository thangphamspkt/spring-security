package com.stc.security;

import com.stc.security.entities.User;
import com.stc.security.repositories.UserRepository;
import com.stc.security.utils.EnumRole;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.scheduling.annotation.EnableAsync;

import java.util.Arrays;

@Slf4j
@EnableAsync
@EnableEurekaClient
@SpringBootApplication
public class SpringSecurityApplication implements CommandLineRunner {

    @Autowired
    private UserRepository userRepository;

    public static void main(String[] args) {
        SpringApplication.run(SpringSecurityApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        if (userRepository.count() == 0) {
            User user = new User("thangpx@hcmute.edu.vn", "123456",
                    Arrays.asList(EnumRole.ROLE_ADMIN.name(), EnumRole.ROLE_USER.name()));
            userRepository.save(user);
        }
    }
}
