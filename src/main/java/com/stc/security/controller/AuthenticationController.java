package com.stc.security.controller;

import com.stc.security.dtos.AccountDto;
import com.stc.security.dtos.TokenDetail;
import com.stc.security.securities.CustomUserDetailService;
import com.stc.security.securities.JwtTokenUtils;
import com.stc.security.securities.JwtUserDetail;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;

/**
 * Created by: IntelliJ IDEA
 * User      : thangpx
 * Date      : 3/29/23
 * Time      : 8:13 AM
 * Filename  : AuthenticationController
 */
@RestController
@RequestMapping("/rest/login")
public class AuthenticationController {
    private final CustomUserDetailService customUserDetailService;

    private final JwtTokenUtils jwtTokenUtils;

    public AuthenticationController(CustomUserDetailService customUserDetailService,
                                    JwtTokenUtils jwtTokenUtils) {
        this.customUserDetailService = customUserDetailService;
        this.jwtTokenUtils = jwtTokenUtils;
    }

    @PostMapping
    public ResponseEntity<TokenDetail> login(@Valid @RequestBody AccountDto dto) {
        final JwtUserDetail userDetails = customUserDetailService
                .loadUserByUsername(dto.getUsername());
        final TokenDetail result = jwtTokenUtils.getTokenDetail(userDetails);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }


    @GetMapping("/hello")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<String> sayHello(Principal principal) {
        return new ResponseEntity<>(String.format("Hello %s", principal.getName()), HttpStatus.OK);
    }
}
