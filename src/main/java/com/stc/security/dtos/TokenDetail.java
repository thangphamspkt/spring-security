package com.stc.security.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by: IntelliJ IDEA
 * User      : thangpx
 * Date      : 3/29/23
 * Time      : 8:42 AM
 * Filename  : TokenDetails
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TokenDetail {
    private String token;

    private long expired;

    private List<String> roles = new ArrayList<>();
}
