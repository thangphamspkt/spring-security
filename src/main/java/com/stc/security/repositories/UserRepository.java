package com.stc.security.repositories;

import com.stc.security.entities.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.Optional;

/**
 * Created by: IntelliJ IDEA
 * User      : thangpx
 * Date      : 3/29/23
 * Time      : 8:02 AM
 * Filename  : UserRepository
 */
public interface UserRepository extends MongoRepository<User, String> {

    @Query(value = "{'email': ?0}")
    Optional<User> getUser(String email);

}
