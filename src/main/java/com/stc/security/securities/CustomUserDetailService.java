package com.stc.security.securities;

import com.stc.security.entities.User;
import com.stc.security.exceptions.InvalidException;
import com.stc.security.repositories.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

/**
 * Created by: IntelliJ IDEA
 * User      : thangpx
 * Date      : 3/29/23
 * Time      : 8:03 AM
 * Filename  : CustomUserDetailService
 */
@Slf4j
@Service
public class CustomUserDetailService implements UserDetailsService {

    private final UserRepository userRepository;

    public CustomUserDetailService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public JwtUserDetail loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userRepository.getUser(email).orElseThrow(() ->
                new UsernameNotFoundException(String.format("Tài khoản có email %s không tồn tại", email)));
        if (!user.isTrangThai()) {
            throw new InvalidException("Tài khoản đã bị khóa, vui lòng liên hệ quản trị viên");
        }
        return new JwtUserDetail(
                user.getEmail(),
                user.getPassword(),
                user.getRoles().stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList()),
                user.isTrangThai());
    }
}
