package com.stc.security.utils;

/**
 * Created by: IntelliJ IDEA
 * User      : thangpx
 * Date      : 3/29/23
 * Time      : 8:10 AM
 * Filename  : EnumRole
 */
public enum EnumRole {
    ROLE_ADMIN,
    ROLE_USER
}
